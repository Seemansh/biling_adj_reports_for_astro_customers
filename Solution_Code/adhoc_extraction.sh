#!/usr/bin/sh
#***************************************************************************************
# Maxis Telecom
# Filename: Billing_And_Adj_Rpt_For_ASTRO_IPTV_Wrapper.sh
# ******************	Version : 1.0 **************************************************
# Execution Syntax: nohup Billing_And_Adj_Rpt_For_ASTRO_IPTV_Wrapper.sh &
# Example:nohup Billing_And_Adj_Rpt_For_ASTRO_IPTV_Wrapper.sh &
#****************************************************************************************

##----- Report Main Configuration File ----------------------------
. /home/RPTDUMP/repdev/BILLING_AND_ADJ_RPT_FOR_ASTRO_IPTV/CODE/init.ini

##----- Program main variables section ----------------------------
pdate=`date +"%Y%m%d%H%M%S"`
main_logfile=$report_log/Billing_And_Adj_Rpt_For_ASTRO_IPTV_Wrapper_${pdate}.log

##*********Checking whether the current or related process already in progress or not*************

check_runp=`ps -ef | grep Billing_And_Adj_Rpt_For_ASTRO_IPTV_Wrapper.sh | grep -v vi |  grep -v grep | wc -l`
sleep 2
if [ ${check_runp} -gt 1  ]; then
        echo "Process Billing_And_Adj_Rpt_For_ASTRO_IPTV_Wrapper.sh is already in progress" >> $main_logfile
        exit 1
fi


#******************************************CONNECTION FOR SQL************************************#
#define SQL for connect string
SQL_CONNECT=`echo $ORACLE_HOME/bin/sqlplus -s/nolog $auth_1/$auth_1@$main_inh_db`
export SQL_CONNECT
TRIM_HEAD=`echo "head -4"`
export TRIM_HEAD
TRIM_TAIL=`echo "tail -1"`
export TRIM_TAIL

echo "`date +"%Y-%m-%d.%H:%M:%S"` | STEPS | Validate Input Parameters" >> $main_logfile
#******************************************************************************************************
# CHECK INPUT PARAMETERS & DATELOGIC
#******************************************************************************************************

if [ $# -eq 0 ]; then

	START_DT=`echo "SELECT TO_CHAR(LAST_DAY(ADD_MONTHS(SYSDATE,-2))+1,'DD-MON-YYYY') FROM DUAL;"`
	START_DT=`((echo $START_DT | $SQL_CONNECT) | $TRIM_HEAD) | $TRIM_TAIL`
	export START_DT
	echo "START_DT: $START_DT"

	END_DT=`echo "SELECT TO_CHAR(LAST_DAY(ADD_MONTHS(SYSDATE,-1)),'DD-MON-YYYY') FROM DUAL;"`
	END_DT=`((echo $END_DT | $SQL_CONNECT) | $TRIM_HEAD) | $TRIM_TAIL`
	export END_DT
	echo "END_DT  : $END_DT"

elif [ $# -eq 1 ]; then

	START_DT=`echo "SELECT TO_CHAR(LAST_DAY(ADD_MONTHS(TO_DATE('$1','DD-MON-YYYY'),-1))+1,'DD-MON-YYYY') FROM DUAL;"`
	START_DT=`((echo $START_DT | $SQL_CONNECT) | $TRIM_HEAD) | $TRIM_TAIL`
	export START_DT
	echo "START_DT: $START_DT"

	END_DT=`echo "SELECT TO_CHAR(LAST_DAY(TO_DATE('$1','DD-MON-YYYY')),'DD-MON-YYYY') FROM DUAL;"`
	END_DT=`((echo $END_DT | $SQL_CONNECT) | $TRIM_HEAD) | $TRIM_TAIL`
	export END_DT
	echo "END_DT  : $END_DT"

elif [ $# -gt 1  ] ; then

	echo "`date +"%Y-%m-%d.%H:%M:%S"` | ERROR | Invalid number of arguments passed" >> $main_logfile
	echo "`date +"%Y-%m-%d.%H:%M:%S"` | PROCESS END | Script Terminated" >> $main_logfile

        echo "Invalid Parameters | Usage : Billing_And_Adj_Rpt_For_ASTRO_IPTV_Wrapper.sh Terminated..."
	exit 1
fi
#******************************************************************************************************
#  Obtaining GSMs Names
#******************************************************************************************************
server_num=5
for db_conn in ${main_db_links[@]}
do
    db_links[$server_num]=`echo $db_conn`
    gsm_links_process[${server_num}-5]=$!
    ((server_num=server_num+1))
done

wait_process 'batch_gsm_links' $main_logfile ${gsm_links_process[@]}

echo "CHECKING DB LINKS: ${db_links[5]} ${db_links[6]} ${db_links[7]} ${db_links[8]} ${db_links[9]}"
 
#******************************************************************************************************
# Program Start
#******************************************************************************************************
echo "`date +"%Y-%m-%d.%H:%M:%S"` | PROCESS START | Maxis Extraction Batch Execution - Started" >> $main_logfile

#******************************************************************************************************
# INHOUSE DB PROCESSING(EXTRACTION)
#******************************************************************************************************
 echo "`date +"%Y-%m-%d.%H:%M:%S"` | STEPS | Extracting Output Reports from MXPINH" >> $main_logfile
  . $report_code/mxs_mig_extraction_exe.sh EXTRACTION_SQL $main_inh_db &
  report_extract_process=$!
 wait_process 'report_extraction' $main_logfile ${report_extract_process}
  echo "`date +"%Y-%m-%d.%H:%M:%S"` | PROCESS END | Maxis Extraction Batch Execution - Completed For File : "$fil >> $main_logfile


# Program Completed
echo "`date +"%Y-%m-%d.%H:%M:%S"` | PROCESS END | Script Completed" >> $main_logfile
#*******************************************************************************************************
# END OF EXTRACTION PROGRAM
#*******************************************************************************************************