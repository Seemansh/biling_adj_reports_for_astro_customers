SELECT DISTINCT
  EXTERNAL_ACCOUNT_NO
  ||','
  ||ADJ_ACCOUNT_NO
  ||','
  ||SUBSCR_NO
  ||','
  ||SUBSCR_NO_RESETS
  ||','
  ||ADJ_TRANS_CODE
  ||','
  ||REPLACE(REPLACE(REPLACE(REPLACE(DESCRIPTION_TEXT,chr(13),' '),chr(10),' '),chr(34),' '),',',' ')
  ||','
  ||ANNOTATION
  ||','
  ||TRANSACT_DATE 
  ||','
  ||EFFECTIVE_DATE
  ||','
  ||TRACKING_ID
  ||','
  ||TRACKING_ID_SERV
  ||','
  ||ORIG_BILL_REF_NO
  ||','
  ||ORIG_BILL_REF_RESETS
  ||','
  ||ORIGINAL_BILL_STATEMENT_DATE
  ||','
  ||BI_BILL_REF_NO
  ||','
  ||BI_BILL_REF_RESETS
  ||','
  ||STATEMENT_DATE
  ||','
  ||TOTAL_AMT
  ||','
  ||OTHER_TAX
  ||','
  ||' '
  ||','
  ||' '
FROM MXS_BILL_ADJ_ASTRO_ORIG_DATE;