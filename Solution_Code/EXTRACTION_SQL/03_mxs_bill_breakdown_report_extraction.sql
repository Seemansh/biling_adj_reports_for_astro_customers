SELECT DISTINCT
    BI_ACCOUNT_NO
  ||','
  ||BI_BILL_REF_NO
  ||','
  ||BI_BILL_REF_RESETS
  ||','
  ||STATEMENT_DATE
  ||','
  ||REPLACE(REPLACE(REPLACE(REPLACE(PACKAGE_NAME,chr(13),' '),chr(10),' '),chr(34),' '),',',' ')
  ||','
  ||SUBTYPE_CODE
  ||','
  ||REPLACE(REPLACE(REPLACE(REPLACE(ELEMENT_NAME,chr(13),' '),chr(10),' '),chr(34),' '),',',' ')
  ||','
  ||SUB_COUNT
  ||','
  ||DISCOUNT_PERCENTAGE
  ||','
  ||TOTAL_BILL_AMOUNT
  ||','
  ||TOTAL_NON_TAXABLE_CHARGE
  ||','
  ||TOTAL_TAXABLE_CHARGE
  ||','
  ||TOTAL_TAX 
FROM MXS_BILL_ADJ_ASTRO_BILL_BRKDWN;