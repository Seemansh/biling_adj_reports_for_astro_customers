CREATE TABLE MXS_BILL_ADJ_ASTRO_TY_CD_2_3 NOLOGGING NOCACHE PCTFREE 0 AS
SELECT DISTINCT 
  AA.HIERARCHY_ID,
  AA.BALANCE_ACCOUNT_NO,
  AA.BI_ACCOUNT_NO,
  AA.BI_BILL_REF_NO,
  AA.BI_BILL_REF_RESETS,
  AA.BI_ORIG_BILL_REFNO,
  AA.BI_ORIG_BILL_REF_RESETS,
  AA.STATEMENT_DATE,
  AA.BILL_INVOICE_ROW,
  AA.TAX_PKG_INST_ID,
  AA.AMOUNT,
  AA.DISCOUNT,
  AA.TAX,
  AA.PACKAGE_ID,
  AA.TYPE_CODE,
  AA.SUBTYPE_CODE,
  AA.COMPONENT_ID,
  AA.PROVIDER_ID,
  AA.ELEMENT_ID,
  AA.PRODUCT_LINE_ID
FROM MXS_BILL_ADJ_ASTRO_BID_DUMP AA
WHERE TYPE_CODE IN (2,3);

CREATE INDEX IND1_BILL_ADJ_ASTRO_TY_CD_2_3 ON MXS_BILL_ADJ_ASTRO_TY_CD_2_3(BI_ACCOUNT_NO);
CREATE INDEX IND2_BILL_ADJ_ASTRO_TY_CD_2_3 ON MXS_BILL_ADJ_ASTRO_TY_CD_2_3(BI_BILL_REF_NO,BI_BILL_REF_RESETS);