CREATE TABLE MXS_BILL_ADJ_ASTRO_TY_23_AMT NOLOGGING NOCACHE PCTFREE 0 AS
SELECT 
  AA.BI_ACCOUNT_NO,
  AA.BI_BILL_REF_NO,
  AA.BI_BILL_REF_RESETS ,
  SUM(NVL(AA.AMOUNT,0))  /100 TOTAL_AMOUNT,
  SUM(NVL(AA.DISCOUNT,0))/100 TOTAL_DISCOUNT,
  (SUM(NVL(AA.AMOUNT,0)) /100 + SUM(NVL(AA.DISCOUNT,0))/100) TOTAL_BILL_AMOUNT,
  SUM(NVL(AA.TAX,0))  /100 TOTAL_TAX,
  COUNT(COMPONENT_ID) SUB_COUNT
FROM MXS_BILL_ADJ_ASTRO_TY_CD_2_3 AA
GROUP BY AA.BI_ACCOUNT_NO,
  AA.BI_BILL_REF_NO,
  AA.BI_BILL_REF_RESETS;

CREATE INDEX IND1_BILL_ADJ_ASTRO_TY_23_AMT ON MXS_BILL_ADJ_ASTRO_TY_23_AMT(BI_ACCOUNT_NO);
CREATE INDEX IND2_BILL_ADJ_ASTRO_TY_23_AMT ON MXS_BILL_ADJ_ASTRO_TY_23_AMT(BI_BILL_REF_NO,BI_BILL_REF_RESETS);