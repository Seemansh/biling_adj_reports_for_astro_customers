CREATE TABLE MXS_BILL_ADJ_ASTRO_BID_DUMP NOLOGGING NOCACHE PCTFREE 0 AS
SELECT DISTINCT 
  AA.*,
  BID.BILL_INVOICE_ROW,
  BID.TAX_PKG_INST_ID,
  BID.AMOUNT,
  BID.DISCOUNT,
  BID.TAX,
  BID.COMPONENT_ID,
  BID.PACKAGE_ID,
  BID.PROVIDER_ID,
  BID.ELEMENT_ID,
  BID.PRODUCT_LINE_ID,
  BID.TYPE_CODE,
  BID.SUBTYPE_CODE
FROM MXS_BILL_ADJ_ASTRO_BILL_INFO AA,
  BILL_INVOICE_DETAIL BID
WHERE AA.BI_BILL_REF_NO   = BID.BILL_REF_NO
AND AA.BI_BILL_REF_RESETS = BID.BILL_REF_RESETS
AND BID.TYPE_CODE        IN (2,3,5);

CREATE INDEX ID1_BILL_ADJ_ASTRO_BID_DUMP ON MXS_BILL_ADJ_ASTRO_BID_DUMP(TYPE_CODE,SUBTYPE_CODE);